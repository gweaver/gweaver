# Gabe's README

:wave: Hello! I'm Gabe Weaver and I'm the Principal Product Manager, Plan:Project Management.

- [LinkedIn](https://www.linkedin.com/in/gabeweaver/)
- [Twitter](https://twitter.com/gabeweaver)
- [GitLab](https://gitlab.com/gweaver)
- [Personal website](http://gabeweaver.com/)
- [Personal feedback form](https://docs.google.com/forms/d/e/1FAIpQLSdUfslqfOrBydpog7vuVa0lGZz_qkBpwkKq2iKNmx_GZfHeIA/viewform?usp=sf_link)

## Personality insights

- [Teacher / Provider](https://gitlab.com/gweaver/workspace) via StandoutReport
- [Campaigner (ENFP-A)](https://www.16personalities.com/enfp-personality) via 16personalities
- [Challenger (8)](https://www.enneagraminstitute.com/type-8) / [Helper (2)](https://www.enneagraminstitute.com/type-2) via Enneagram

## Recommended reading

I often get asked for a list of recommended books to read. To be more efficient, I've created a centralized list that I keep up to date. [Check it out](https://gitlab.com/gweaver/workspace/-/blob/master/books.md).

## Personal context

I live in [Lookout Mountain, Tennessee](https://www.google.com/maps/place/Lookout+Mountain,+TN+37350/data=!4m2!3m1!1s0x88605c7f3c96749d:0x323769a94b1a2b4f?sa=X&ved=2ahUKEwjIy8_T257wAhXHaM0KHSnHAIcQ8gEwJnoECE4QAQ), have been married to the love of my life (Libby) for over 15 years, and have three kids -- Landon (12), Piper (11), and Josie (8). True to what some of the personality tests I've taken have said, I think the world is an amazing place with endless opportunities, and that everyone has the power to positively impact our world for the better. 

I've been doing Product Management in some form or fashion for 14 years and have 6 years of experience as a people manager. Before joining GitLab, I was a co-owner of a remote IoT and data science / machine learning product development firm where I served as the Head of Product, CFO, and Board Member. This was a wonderful experience for me as I had the opportunity to build a generative culture, experiment with different forms of organizational governance, and contribute to a team that produced [disproportianate results](https://www.inc.com/profile/very) for our customers and our business. Prior to that, I was the VP of Product for another [product development firm](https://gofullstack.com/) that specialized in building mobile and web applications. I joined GitLab because I [love the mission](https://about.gitlab.com/company/mission/), strongly align with [the values](https://about.gitlab.com/handbook/values/), and think that effectively delivering on the value proposition of a single application for all of DevOps is an enormous challenge, that if solved well, will drammatically increase the rate at which we can all make the world a better place. 

## Working with me

- I am strongly guided by my values. One of the main reasons I joined GitLab was because our [values](https://about.gitlab.com/handbook/values/) align closely to my own when it comes to the workplace. 
- I communicate with [directness](https://about.gitlab.com/handbook/values/#directness) and often have strong opinions. I will always change my perspective when presented with compelling evidence or a better direction, so I welcome [dissenting opinions](https://about.gitlab.com/handbook/values/#value-dissent) with open arms.
- While I'm direct, you'll find that I care deeply about those that I work with and will go out of my way to support you in any way I can. Please don't be shy about asking for my help. 
- Sometimes I get feedback that I do not smile enough, look unhappy, or seem annoyed. This is rarely the case. I fall on the [neurodiverse spectrum](https://about.gitlab.com/handbook/values/#neurodiversity) and have to actively work to help my body language accurately reflect what is going on in my heart and mind. Please call me out when my body language is creating a negative environment for you when we are collaborating together. 
- More than anything, I want to those around me be successful. I need ongoing feedback on how to improve myself so I can better achieve this. When something I can improve upon is top of mind, please share it with me. 
- I am not afraid to challenge the status quo. I don't believe in sacred cows and I have a strong disdain for "office politics" as I believe they are more often than not selfishly motivated. 
- I believe everyone has something unique to contribute and it's my desire to create space for people to bring their strengths and perspectives to the table. 
- I'm not a micromanager and trust those I work with to get the job done. Sometimes this is perceived as being "hands off". I've found striking the right balance challenging at times because each person is different. If you want me to be more involved in a given discussion, just ask! If I'm too involved, please tell me. 
- I've gotten feedback that sometimes I am too proactive in jumping into a conversation or providing insights in domains that I do not own. Please interpret this solely as me trying to be helpful because I care. I will continue working on my tact and approach for sharing my perspective in the most meaningful way within the given context. 

